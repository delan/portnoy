.POSIX:

dest          = $$(cd && pwd)/bin
test          = dc=comssa,dc=org,dc=au
p_openldap    = openldap-2.4.44
s_openldap    = ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release
out           = build/_ldapclean \
                build/curtin2name build/curtin2given build/curtin2family \
                build/curtin3name build/curtin3given build/curtin3family \
                build/name2curtin build/name2student build/name2staff \
                build/name3curtin build/name3student build/name3staff
path          = s/@@@@@@@/$$(pwd | sed 's/[&/\]/\\&/g')/
strip         = $$(echo "$$i" | sed 's/build\///')
z             = [ 	]

all:                    $(out) external/src/.$(p_openldap).test

install:                all
	find . -type d -exec chmod a+x '{}' \+
	chmod a+rx $(out)
	mkdir -p     "$(dest)"
	chmod a+x    "$(dest)"
	cp -P $(out) "$(dest)"
	for i in $(out); do chmod a+rx "$(dest)/$(strip)"; done

uninstall:
	for i in $(out); do rm -f "$(dest)/$(strip)"; done
	rmdir -p "$(dest)" > /dev/null 2>&1 || true

build:
	mkdir -p build

build/_ldapclean:       build source/_ldapclean
	cp -P source/_ldapclean '$@'

build/curtin2name:      build source/curtin2name
	sed "$(path)" < source/curtin2name > '$@'

build/curtin2given:     build build/curtin2name
	ln -s curtin2name '$@'

build/curtin2family:    build build/curtin2name
	ln -s curtin2name '$@'

build/curtin3name:      build build/curtin2name
	ln -s curtin2name '$@'

build/curtin3given:     build build/curtin2name
	ln -s curtin2name '$@'

build/curtin3family:    build build/curtin2name
	ln -s curtin2name '$@'

build/name2curtin:      build source/name2curtin
	sed "$(path)" < source/name2curtin > '$@'

build/name2student:     build build/name2curtin
	ln -s name2curtin '$@'

build/name2staff:       build build/name2curtin
	ln -s name2curtin '$@'

build/name3curtin:      build build/name2curtin
	ln -s name2curtin '$@'

build/name3student:     build build/name2curtin
	ln -s name2curtin '$@'

build/name3staff:       build build/name2curtin
	ln -s name2curtin '$@'

external/src/.$(p_openldap).test: external/bin/ldapwhoami
	cd external && echo '$(test)' | \
		sed     's/%/%25/g;s/,/%2C/g;s/=/%3D/g' | \
		sed     's/^/ldap:\/\/\//' | \
		tr '\n' '\0' | \
		xargs -0 bin/ldapwhoami -x -H \
		>       src/.$(p_openldap).test || \
		( rm -f src/.$(p_openldap).test && false )

external/bin/ldapwhoami: external/src/$(p_openldap)/Makefile
	cd external/src/$(p_openldap) && $(MAKE) depend
	cd external/src/$(p_openldap) && $(MAKE)
	cd external/src/$(p_openldap) && $(MAKE) install

external/src/$(p_openldap)/Makefile: external/src/.$(p_openldap).configure
	cd external/src/$(p_openldap) && cat ../.$(p_openldap).configure | \
		xargs -0 ./configure --prefix="$$(cd ../.. && pwd)"

external/src/.$(p_openldap).configure: external/src/$(p_openldap)/configure
	cd external/src/$(p_openldap) && ./configure --help | \
		grep -E '^$z+(--(en|dis)able-|--with(out)?-)' | \
		sed  -E 's/^$z+//;s/($z|\[).*$$//' | \
		grep -Ev        '^--(en|dis)able-FEATURE$$' | \
		grep -Ev        '^--with(out)?-(PACKAGE|subdir=DIR)$$' | \
		grep -Ev        '^--with(out)?-(odbc|tags)$$' | \
		sed  -E 's/^--enable-/--disable-/;s/^--with-/--without-/' | \
		sed  -E 's/^--disable-libtool-lock$$/--enable-libtool-lock/' | \
		sort -u | \
		tr '\n' '\0' > ../.$(p_openldap).configure

external/src/$(p_openldap)/configure: external/src/$(p_openldap).tgz
	cd external/src && tar -xpzf $(p_openldap).tgz
	cd external/src && touch $(p_openldap)/configure # HACK!

external/src/$(p_openldap).tgz:
	mkdir -p external/src # HACK!
	cd external/src && echo '$(s_openldap)/$(p_openldap).tgz' | \
		tr '\n' '\0' | \
		xargs -0 curl -O

clean:
	rm -rf build external

.PHONY: all install uninstall clean
